from flask import Flask, redirect, url_for, request

app = Flask(__name__)
@app.route('/ping',methods = ['POST', 'GET'])
def ping():
   if request.method == 'POST':
      return 'pong'
   else:
      return 'error'
@app.errorhandler(404)
def page_not_found(e):
      return 'error'
